package tv.codely.java_bootstrap;

import java.util.Objects;

public class WordCount {
    private final int count;

    public WordCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WordCount)) return false;
        WordCount wordCount = (WordCount) o;
        return getCount() == wordCount.getCount();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCount());
    }
}
