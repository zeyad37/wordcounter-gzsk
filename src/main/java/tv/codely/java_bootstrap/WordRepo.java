package tv.codely.java_bootstrap;

import java.util.List;

public class WordRepo implements IRepo {

    private List<Word> skip;

    public WordRepo(List<Word> skip) {
        this.skip = skip;
    }

    @Override
    public List<Word> getSkip() {
        return skip;
    }

    public void setSkip(List<Word> skip) {
        this.skip = skip;
    }
}
