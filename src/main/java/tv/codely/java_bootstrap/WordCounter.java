package tv.codely.java_bootstrap;

import java.util.List;

public class WordCounter {
    IRepo wordRepo;

    public WordCounter(IRepo wordRepo) {
        this.wordRepo = wordRepo;
    }

    WordCount getWordCount(Phrase phrase) {
        return new WordCount(getCount(phrase));
    }

    public int getCount(Phrase phrase) {
        if (checkIsEmpty(phrase)) return 0;
        String[] split = phrase.getPhrase().split(" ");
        List<Word> skipableList = wordRepo.getSkip();
        int result = 0;
        for (String word : split) {
            if (!skipableList.contains(new Word(word)))
                result++;
        }
        return result;
    }

    public WordCount getVowelWordCount(Phrase phrase) {
        if (checkIsEmpty(phrase)) return new WordCount(0);
        String[] split = phrase.getPhrase().split(" ");
        int result = 0;
        for (String word : split) {
            String lowerCase = word.toLowerCase();
            if (lowerCase.startsWith("a") || lowerCase.startsWith("e") || lowerCase.startsWith("i") || lowerCase.startsWith("o")
                    || lowerCase.startsWith("u"))
                result++;
        }
        return new WordCount(result);
    }

    public WordCount getThreeLetterWordCount(Phrase phrase) {
        if (checkIsEmpty(phrase)) return new WordCount(0);
        String[] split = phrase.getPhrase().split(" ");
        int result = 0;
        for (String word : split) {
            if (word.length() > 3)
                result++;
        }
        return new WordCount(result);
    }

    private boolean checkIsEmpty(Phrase phrase) {
        return phrase.getPhrase().isEmpty();
    }
}