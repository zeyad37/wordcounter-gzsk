package tv.codely.java_bootstrap;

import java.util.Collections;
import java.util.Scanner;

public class Greeter {

    public String greet(String name) {
        return "Hello " + name;
    }

    public static void main(String[] args) {
        WordRepo wordRepo = new WordRepo(Collections.singletonList(new Word("Hello")));
        WordCounter wordCounter = new WordCounter(wordRepo);
        Scanner in = new Scanner(System.in);
        String line = in.nextLine();
        System.out.println(wordCounter.getWordCount(new Phrase(line)));
    }

}
