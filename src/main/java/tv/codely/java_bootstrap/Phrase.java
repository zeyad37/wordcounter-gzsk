package tv.codely.java_bootstrap;

public class Phrase {
    private final String phrase;

    public Phrase(String phrase) {
        this.phrase = phrase;
    }

    public String getPhrase() {
        return phrase;
    }
}
