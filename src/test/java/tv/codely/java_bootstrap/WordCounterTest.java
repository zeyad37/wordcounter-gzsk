package tv.codely.java_bootstrap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class WordCounterTest {
    WordCounter wordCounter;
    IRepo wordRepo;
    @BeforeEach
    public void before() {
        wordRepo = new WordRepo(Collections.singletonList(new Word("Hello")));
        wordCounter = new WordCounter(wordRepo);
    }

    @Test
    void countWordsIfEmpty() {
        assertEquals(new WordCount(0), wordCounter.getWordCount(new Phrase("")));
    }

    @Test
    void countWordsIfNotEmpty() {
        assertEquals(new WordCount(2), wordCounter.getWordCount(new Phrase("Hello John Doe")));
    }

    @Test
    void countWordsStartingWithVowelsIfNone() {
        assertEquals(new WordCount(0), wordCounter.getVowelWordCount(new Phrase("Hello John Doe")));
    }

    @Test
    void countWordsStartingWithVowels() {
        assertEquals(new WordCount(3), wordCounter.getVowelWordCount(new Phrase("AHello Eohn IDoe")));
    }

    @Test
    void countThreeLetterWords() {
        assertEquals(new WordCount(3), wordCounter.getThreeLetterWordCount(new Phrase("AHello Eohn IDoe")));
    }
}